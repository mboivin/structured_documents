# XML documents

- [<dh> Digital humanities](http://dh.obdurodon.org/)
- [What is XML and why should humanists care? An even gentler introduction to XML](http://dh.obdurodon.org/what-is-xml.xhtml)
- [XML en M1](https://www.irif.fr/~carton/Enseignement/XML/)
- [Cours de XML](http://www.gchagnon.fr/cours/xml/index.html)
- [Looking at the Forest Instead of the Trees](http://www.iro.umontreal.ca/~lapalme/ForestInsteadOfTheTrees/HTML/index.html)

## XML - DTD

```sh
xmllint --dtdvalid file.dtd file.xml
```

## RELAX NG

- DocBook, TEI, OpenDocument, Epub
- [RELAX NG](http://www.iro.umontreal.ca/~lapalme/ForestInsteadOfTheTrees/HTML/ch03s03.html)
