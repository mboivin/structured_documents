#!/usr/bin/env python3

import sys
import csv
from lxml import etree as ET
from lxml.builder import E


# Common elements
TOILETTE = E.toilette
ADR = E.adresse
LIB = E.libelle
ARR = E.arrondissement
HORAIRE = E.horaire
SERV = E.services
ACC_PMR = E.acces_pmr
REL_BEBE = E.relais_bebe
EQU = E.equipement


def fill_toilette_data(_libelle, _arr, _horaire, _pmr, _bebe, _equ, _type, _statut=""):
    """Fills data of a toilette element

    Returns:
        A lxml.etree._Element
    """
    toilette = (
        TOILETTE(
            ADR(
                LIB(_libelle),
                ARR(_arr)
            ),
            HORAIRE(_horaire),
            SERV(
                ACC_PMR(_pmr),
                REL_BEBE(_bebe)
            ),
            EQU(_equ),
            type=_type, statut=_statut
        )
    )
    if not _statut: # implied
        del toilette.attrib["statut"]
    return toilette


def convert_csv_to_xml(input_csv, output_xml):
    """Converts a given csv file to xml
    
    Args:
        input_csv: the input csv file to convert
        output_xml: the name of the output file
    """
    root = ET.Element("toilettes")

    with open(input_csv) as f:
        next(f) # skip header

        csv_reader = csv.reader(f, delimiter=";")

        for row in csv_reader:
            root.append(fill_toilette_data(row[2], row[3], row[4], row[5], row[6], row[7], row[0], row[1]))

    tree_str = ET.tostring(root,
                           encoding="UTF-8",
                           xml_declaration=True,
                           pretty_print=True,
                           doctype='<!DOCTYPE toilettes SYSTEM "wc.dtd">')

    with open(output_xml, "wb") as f:
        f.write(tree_str)


def main(argv):
    if len(argv) == 3:
        convert_csv_to_xml(argv[1], argv[2])
    else:
        print(f"Usage: python {argv[0]} <input_csv> <output_xml>")
    return 0


if __name__ == "__main__":
    main(sys.argv)
